import * as yup from "yup";

const regSchema = yup.object().shape({
	name: yup
		.string()
		.required("Please enter your name")
		.matches(/^[a-zA-Zçéèëêîïä'\- ]*$/, "Please enter a correct name"),
	email: yup
		.string()
		.required("Please provide your email")
		.email("Email doesn't look right")
		.matches(
			/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i,
			"Please provide a correct email"
		),
	message: yup
		.string()
		.required("Please enter your message")
		.matches(
			/^[a-zA-Zçéèëêîïä,.?:!;%$€()="'&\- ]*$/,
			"Please enter a correct message"
		),
});

export { regSchema };
